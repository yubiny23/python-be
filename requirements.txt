Flask==2.0.1
Werkzeug==2.0.1
Flask_RESTful==0.3.9
Flask_Cors==3.0.10
Flask_SQLAlchemy==2.5.1
secrets==1.0.2
