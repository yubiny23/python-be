from flask import Flask, render_template, flash, request, redirect, url_for, session
from flask import jsonify
from flask_session import Session


from flask_sqlalchemy import SQLAlchemy


from flask_cors import CORS, cross_origin
import datetime

#importamos la conexion
from projectconfig import conn
from proyecto import db
from proyecto import *

#sistema
import secrets
import os

import logging
logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


import hashlib

#genero el sha256
def encrypt_string(hash_string):
    sha_signature = \
        hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature


#create the object of Flask
app  = Flask(__name__)
app.config['SECRET_KEY'] = '916f4c31aaa35d6b867dae9a7f54270d'
#app.permanent_session_lifetime = datetime.timedelta(minutes=60)
#app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"

app.config.update(SECRET_KEY=os.urandom(24))

#app.config.update(SESSION_COOKIE_SAMESITE="None", SESSION_COOKIE_SECURE=True)
Session(app)
CORS(app, supports_credentials=True)


#SqlAlchemy Database Configuration With Mysql
app.config['SQLALCHEMY_DATABASE_URI'] = conn
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)



#creating our routes
@app.route('/')
def index():
    
    return jsonify(
                    {"message": "Root", "severity": "info"}
                )



#Empresas rol - rutas 
@app.route('/empresasrol/<id>' , methods = ['GET'])
def empresasrol_get(id):
    if request.method == 'GET':


        try:
            u = BtEmpresaRol.query.filter_empresasrolby(id_empresa_rol=id).first()
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            db.session.commit()
            return jsonify(
                            {
                                "id_empresa_rol": u.id_empresa_rol ,
                                "rol" : u.rol, 
                                "fecha": u.fecha_creacion
                            }
                        )
        except:
            return jsonify({ "id_empresa_rol" : "0"})

@app.route('/empresasrola' , methods = ['GET'])
def empresasrol_all():
    if request.method == 'GET':
        try:
            u = db.session.query(BtEmpresaRol).all()
            
            a = list()
            for rol in u:
                a.append({ 
                    "id_empresa_rol" : rol.id_empresa_rol,
                    "rol" : rol.rol,
                    "fecha_creacion" : rol.fecha_creacion, 
                    })
            
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "id_empresa_rol" : "0"})
    

@app.route('/empresasrol' , methods = ['POST'])
def empresasrol_crear():
    if request.method == 'POST':
        gRol = request.form.get('rol')    
        
        try:
            u = BtEmpresaRol(rol=gRol)
            db.session.add(u)
            db.session.commit()
            return jsonify(
                            {"id": u.id_empresa_rol, "fecha": u.fecha_creacion }
                        )
        except:
            db.session.rollback()
            return jsonify ( { "error" : "error" } )



@app.route('/empresasrold/<id>' , methods = ['POST'])
def empresasrol_eliminar(id):
    if request.method == 'POST':
        try:

            db.session.query(BtEmpresaRol).filter(BtEmpresaRol.id_empresa_rol==id).delete()
            db.session.commit()
            return jsonify(
                            {"msg": "id eliminado" }
                        )
        except:
            db.session.rollback()
            return jsonify ( { "error" : "error" } )


@app.route('/empresasrole/<id>' , methods = ['POST'])
def empresasrol_modificar(id):
    if request.method == 'POST':
        gRol = request.form.get('rol')
        try:
            a = db.session.query(BtEmpresaRol).filter(BtEmpresaRol.id_empresa_rol==id).one()
            a.rol = gRol
            db.session.commit()
            return jsonify(
                            {"rol": a.rol }
                        )
        except:
            db.session.rollback()
            return jsonify ( { "error" : "error" } )


#Login - rutas 
@app.route('/login' , methods = ['POST'])
def login():
    if request.method == 'POST':
        gUsuario = request.form.get('usuario')
        gPassword = request.form.get('password')
        print("estos son los datos %s , %s" % (gUsuario,gPassword) )
        gActivo = 1;
        #try:
        a = db.session.query(BtUsuario).filter(BtUsuario.correo_electronico==gUsuario).first()
        #<aca>
        #try:
        if a:
            if a.id_estado_usuario == 1 and a.password ==  encrypt_string(gPassword):
                tokenSecret = secrets.token_hex(20)
                gus = ("%s %s") % (a.nombres,a.apellidos)
                
                cv = BtSesiones(
                   
                    token = tokenSecret,
                    usuario = a.id_usuario,
                    nombres = gus,
                    idperfil = a.id_rol,                
                )

                db.session.add(cv)
                db.session.commit()  

                respuesta = { "code" : "200" , "msg" : "Login correcto", "user" : a.id_usuario , "token" : tokenSecret} 

            elif a.id_estado_usuario == 2:
                respuesta = { "code" : "400" , "msg" : "Usuario inactivo" }
            elif a.id_estado_usuario == 3 :
                respuesta =  { "code" : "401" , "msg" : "Usuario bloqueado" }
            else :
                respuesta = {"code" : "404" , "msg" : "usuario incorrecto"}
        else:
            respuesta = {"code" : "404" , "msg" : "usuario incorrecto"}
        
        
        db.session.commit()
        return jsonify(
                        respuesta
                    )
        #except:
        #    db.session.rollback()
        #    return jsonify ( { "error" : "error" } )
        
#peticiones para la gestion de usuarios de la plataforma
@app.route('/usuariosm' , methods = ['GET'])
def usuariosm_all():
    if request.method == 'GET':
        
        u = db.session.query(BtUsuario).all()            
        a = list()
        for rUsuario in u:
            a.append({ 
                "id_usuario" : rUsuario.id_usuario,
                "nombres" : rUsuario.nombres,
                "apellidos" : rUsuario.apellidos,
                "correo" : rUsuario.correo_electronico  ,
                "fecha_creacion" : rUsuario.fecha_creacion, 
                })
        
        #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
        return jsonify(
                        {
                            "code" : "200" ,
                            "datos": a
                        }
                    )
            

#peticiones para la gestion de usuarios de la plataforma
@app.route('/usuariosm' , methods = ['POST'])
def usuariosm_crear():
    if request.method == 'POST':
        gNombres = request.form.get('nombres')
        gApellidos = request.form.get('apellidos')
        gIdRol = request.form.get('id_rol')
        gPassword = request.form.get('password')
        gPasswordE = encrypt_string (gPassword)
        gCorreo = request.form.get('correo')    
        gIdestado_usuario = 1

        try:
            u = BtUsuario(nombres=gNombres,apellidos=gApellidos,id_rol=gIdRol,password=gPasswordE,correo_electronico=gCorreo,id_estado_usuario=gIdestado_usuario)
            db.session.add(u)
            db.session.commit()
            return jsonify(
                            {"id": u.id_usuario, "fecha": u.fecha_creacion }
                        )
        except:
            db.session.rollback()
            return jsonify ( { "error" : "error" } )



@app.route('/usuariosme/<id>' , methods = ['POST'])
def usuariosm_modificar(id):
    if request.method == 'POST':
        gNombres = request.form.get('nombres')
        gApellidos = request.form.get('apellidos')
        gIdRol = request.form.get('id_rol')
        
        #gPassword = request.form.get('password')
        #gPassword2 = request.form.get('password2')

        gCorreo = request.form.get('correo')    
        #gIdestado_usuario = 1

        try:
            a = db.session.query(BtUsuario).filter(BtUsuario.id_usuario==id).one()
            
            #if gPassword == gPassword2 and gPassword != "" :
            #    gPassword3 = encrypt_string(gPassword)  
            #    a.password=gPassword3


            a.nombres=gNombres
            a.apellidos=gApellidos
            a.id_rol=gIdRol
            
            a.correo_electronico=gCorreo
            #a.id_estado_usuario=gIdestado_usuario
            
            db.session.commit()
            return jsonify(
                            {"usuario_modificado": a.id_usuario }
                        )
        except:
            db.session.rollback()
            return jsonify ( { "error" : "error" } )

@app.route('/usuariosme/<id>' , methods = ['GET'])
def usuariosm_obtener(id):
    if request.method == 'GET':

        try:
            a = db.session.query(BtUsuario).filter(BtUsuario.id_usuario==id).one()
            
            db.session.commit()
            return jsonify(
                            {
                                "nombres" : a.nombres,
                                "apellidos" :a.apellidos,
                                "id_rol" : a.id_rol,
                                "correo_electronico" : a.correo_electronico,
                                "id_estado_usuario" : a.correo_electronico
                            }
                        )
        except:
            db.session.rollback()
            return jsonify ( { "error" : "error" } )


@app.route('/usuariosmd/<id>' , methods = ['POST'])
def usuariosm_eliminar(id):
    if request.method == 'POST':
        try:

            a = db.session.query(BtUsuario).filter(BtUsuario.id_usuario==id).one()

            a.id_estado_usuario=4
            
            db.session.commit()
            return jsonify(
                            {"msg": "estado del usuario como eliminado" }
                        )
        except:
            db.session.rollback()
            return jsonify ( { "error" : "error" } )

@app.route('/rolesa' , methods = ['GET'])
def roles_all():
    if request.method == 'GET':
        try:
            u = db.session.query(BtRol).all()
            
            a = list()
            for rol in u:
                a.append({ 
                    "id_rol" : rol.id_rol,
                    "rol" : rol.rol,
                    "fecha_creacion" : rol.fecha_creacion, 
                    })
            
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "id_empresa_rol" : "0"})


#peticiones para la gestion de las empresas
@app.route('/empresasall' , methods = ['GET'])
def empresas_all():
    if request.method == 'GET':
        try:
            u = db.session.query(BtEmpresa).all()
            
            a = list()
            for empresa in u:
                a.append({ 
                    "id_empresa" : empresa.id_empresa,
                    "empresa" : empresa.empresa,
                    "codigo" : empresa.codigo_empresa,
                    "razon_social": empresa.razon_social,
                    "fecha_creacion" : empresa.fecha_creacion, 
                    })
            
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "id_empresa_rol" : "0"})

#Obtener un registro por ID
@app.route('/empresa/<id>' , methods = ['GET'])
def empresa_get(id):
    if request.method == 'GET':
        a = db.session.query(BtEmpresa).filter(BtEmpresa.id_empresa==id).one()
        
        return jsonify(
                            {
                                "empresa" : a.empresa,
                                "razon_social" :a.razon_social,
                                "direccion" : a.direccion,
                                "telefono_1" : a.telefono_1,
                                "giro" : a.giro,
                                "codigo_empresa" : a.codigo_empresa,
                                "fecha_creacion" : a.fecha_creacion,
                            }
                        )
        
        #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
        return jsonify(
                        {
                            "code" : "200" ,
                            "datos": a
                        }
                    )
            

#peticiones para la gestion de los usuarios de las empresas
@app.route('/empresasuall/<id>' , methods = ['GET'])
def empresasu_all(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BtEmpresaUsuario).filter(BtEmpresaUsuario.id_empresa==id).all()

            a = list()
            for empresau in u:
                a.append({ 
                    "id_empresa_usuario" : empresau.id_empresa_usuario,
                    "nombres" : empresau.nombres,
                    "apellidos" : empresau.apellidos,
                    "id_empresa_rol": empresau.id_empresa_rol,
                    "correo_electronico": empresau.correo_electronico,
                    "id_estado_empresa_usuario": empresau.id_estado_empresa_usuario,
                    "id_empresa" : empresau.id_empresa,
                    "fecha_creacion" : empresau.fecha_creacion, 
                    })
        
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "id_empresa_rol" : "0"})

@app.route('/empresausuario/<id>' , methods = ['GET'])
def usuariosempresa_obtener(id):
    if request.method == 'GET':

        try:
            a = db.session.query(BtEmpresaUsuario).filter(BtEmpresaUsuario.id_empresa_usuario==id).one()
            
            db.session.commit()
            return jsonify(
                            {
                                "nombres" : a.nombres,
                                "apellidos" :a.apellidos,
                                "id_rol" : a.id_empresa_rol,
                                "correo_electronico" : a.correo_electronico,
                                "id_estado_empresa_usuario" : a.id_estado_empresa_usuario
                            }
                        )
        except:
            db.session.rollback()
            return jsonify ( { "error" : "error" } )


#peticiones para la gestion de las empresas
@app.route('/curriculumall' , methods = ['GET'])
def curriculum_all():
    if request.method == 'GET':

        try:
            u = db.session.query(BtCurriculum).all()        
            a = list()
            for cv in u:
                a.append({
                    "id_curriculum" : cv.id_curriculum,
                    "id_curriculum_estado" :    cv.id_curriculum_estado,
                    "nombres" :    cv.nombres,
                    "apellidos" :    cv.apellidos,
                    "genero" :    cv.genero,
                    "fecha_nacimiento" :    cv.fecha_nacimiento,
                    "dui_pasaporte" :    cv.dui_pasaporte,
                    "nit" :    cv.nit, 
                    "nup" :    cv.nup,
                    "direccion" :    cv.direccion,
                    "correo_electronico" :    cv.correo_electronico
                    
                    })

            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a,
                                "estado" : "ok",
                                "code" : "200"
                            }
                        )
        except:
            return jsonify({ "datos" : {}})

@app.route('/curriculum/<id>' , methods = ['GET'])
def curriculum_id(id):
    if request.method == 'GET':
        try:
            cv = db.session.query(BtCurriculum).filter(BtCurriculum.id_curriculum==id).one()

            

            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id_curriculum" : cv.id_curriculum,
                    "id_curriculum_estado" :    cv.id_curriculum_estado,
                    "nombres" :    cv.nombres,
                    "apellidos" :    cv.apellidos,
                    "nombre_completo" : ( "%s %s" % (cv.nombres,cv.apellidos) ),
                    "genero" :    cv.genero,
                    "fecha_nacimiento" :    cv.fecha_nacimiento,
                    "dui_pasaporte" :    cv.dui_pasaporte,
                    "nit" :    cv.nit, 
                    "nup" :    cv.nup,
                    "direccion" :    cv.direccion,
                    "correo_electronico" :    cv.correo_electronico,
                    "deporte" :   ("a la persona le gusta el %s" % (cv.deporte) ) ,
                    }})

            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "datos" : {}})


@app.route('/curriculumagregar/' , methods = ['POST'])
def curriculum_agregar():
    if request.method == 'POST':

        nombres = request.form.get('nombres')
        apellidos = request.form.get('apellidos')
        direccion = request.form.get('direccion')
        genero = request.form.get('genero')
        fecha = request.form.get('fecha_nacimiento')
        correo_electronico = request.form.get('correo_electronico')
        dui_pasaporte = request.form.get('dui_pasaporte')
        nit = request.form.get('nit')
        nup = request.form.get('nup')
         

        try:

            cv = BtCurriculum(
                id_curriculum_estado = "1",
                nombres = nombres,
                apellidos = apellidos,
                genero = genero,
                fecha_nacimiento = fecha,
                dui_pasaporte = dui_pasaporte,
                nit = nit,
                nup = nup,
                direccion = direccion,
                correo_electronico = correo_electronico
            )


            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_curriculum
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/curriculumeditar/' , methods = ['POST'])
def curriculum_editar():
    if request.method == 'POST':
        id = request.form.get('id')
        nombres = request.form.get('nombres')
        apellidos = request.form.get('apellidos')
        direccion = request.form.get('direccion')
        genero = request.form.get('genero')
        fecha = request.form.get('fecha_nacimiento')
        correo_electronico = request.form.get('correo_electronico')
        dui_pasaporte = request.form.get('dui_pasaporte')
        nit = request.form.get('nit')
        nup = request.form.get('nup')
         

        try:
            a = db.session.query(BtCurriculum).filter(BtCurriculum.id_curriculum==id).one()
            a.nombres = nombres,
            a.apellidos = apellidos,
            a.genero = genero,
            a.fecha_nacimiento = fecha,
            a.dui_pasaporte = dui_pasaporte,
            a.nit = nit,
            a.nup = nup,
            a.direccion = direccion,
            a.correo_electronico = correo_electronico


            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_curriculum,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

#peticiones para obtener los conocimientos academicos
# conocimientosacademicos
@app.route('/conocimientosacademicos/<id>' , methods = ['GET'])
def conocimientosacademicos_all(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BdConocimientoAcademico).filter(BdConocimientoAcademico.id_persona==id).all()
            
            a = list()
            for cv in u:
                a.append({
                    "id_conocimiento_academico" : cv.id_conocimiento_academico,
                    "titulo" :    cv.titulo,
                    "institucion" :    cv.institucion,
                    "fecha_inicio" :    cv.fecha_inicio,
                    "fecha_fin" :    cv.fecha_fin,
                    "fecha_creacion" : cv.fecha_creacion, 
                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/academicosagregar/' , methods = ['POST'])
def cademico_id_agregar():
    if request.method == 'POST':
        titulo = request.form.get('titulo')
        institucion = request.form.get('institucion')
        fecha_inicio = request.form.get('fecha_inicio')
        fecha_fin = request.form.get('fecha_fin')
        id = request.form.get('idcv')
          
        try:

            cv = BdConocimientoAcademico(
                titulo=titulo,
                institucion=institucion,
                fecha_inicio=fecha_inicio,
                fecha_fin=fecha_fin,
                id_persona=id
            )

            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_conocimiento_academico
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/cacademicosid/<id>' , methods = ['GET'])
def conocimientosacademicos_id(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BdConocimientoAcademico).filter(BdConocimientoAcademico.id_conocimiento_academico==id).one()
            
            return jsonify(
                            {
                                "datos": {
                                        "id_conocimiento_academico" : u.id_conocimiento_academico,
                                        "titulo" :    u.titulo,
                                        "institucion" :    u.institucion,
                                        "fecha_inicio" :    u.fecha_inicio,
                                        "fecha_fin" :    u.fecha_fin,
                                        "fecha_creacion" : u.fecha_creacion, 
                                        "id_persona" : u.id_persona, 
                                        }
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})



@app.route('/academicoseditar/' , methods = ['POST'])
def academicos_editar_id():
    if request.method == 'POST':
        titulo = request.form.get('titulo')
        institucion = request.form.get('institucion')
        fecha_inicio = request.form.get('fecha_inicio')
        fecha_fin = request.form.get('fecha_fin')
        id = request.form.get('id')
         

        try:
            a = db.session.query(BdConocimientoAcademico).filter(BdConocimientoAcademico.id_conocimiento_academico==id).one()
            a.titulo = titulo,
            a.institucion = institucion,
            a.fecha_inicio = fecha_inicio,
            a.fecha_fin = fecha_fin

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_persona,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/academicoseliminar/' , methods = ['POST'])
def cacademicos_id_eliminar():
    if request.method == 'POST':
        idc = request.form.get('id')
         
        try:
            a = db.session.query(BdConocimientoAcademico).filter(BdConocimientoAcademico.id_conocimiento_academico==idc).delete()
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

#peticiones para obtener experiencialaoral
# experiencialaoral
@app.route('/experiencialaoral/<id>' , methods = ['GET'])
def experiencialaoral_all(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BtExperienciaLaboral).filter(BtExperienciaLaboral.id_persona==id).all()
            
            a = list()
            for cv in u:
                a.append({
                    "id_experiencia_laboral" : cv.id_experiencia_laboral,
                    "compania" :    cv.compania,
                    "funciones" :    cv.funciones,
                    "nombre_contrato" :    cv.nombre_contrato,
                    "posicion_contrato" :    cv.posicion_contrato,
                    "telefono_contacto" : cv.telefono_contacto, 
                    "telefono_contacto2" : cv.telefono_contacto2, 
                    "fecha_inicio" : cv.fecha_inicio,
                    "fecha_fin" : cv.fecha_fin
                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/experiencialaoralid/<id>' , methods = ['GET'])
def experiencialaoral_id(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BtExperienciaLaboral).filter(BtExperienciaLaboral.id_experiencia_laboral==id).one()
            
            return jsonify(
                            {
                                "datos": {
                                        "id_experiencia_laboral" : u.id_experiencia_laboral,
                                        "compania" :    u.compania,
                                        "funciones" :    u.funciones,
                                        "nombre_contrato" :    u.nombre_contrato,
                                        "posicion_contrato" :    u.posicion_contrato,
                                        "telefono_contacto" : u.telefono_contacto, 
                                        "telefono_contacto2" : u.telefono_contacto2, 
                                        "fecha_inicio" : u.fecha_inicio,
                                        "fecha_fin" : u.fecha_fin,
                                        "id_persona" : u.id_persona
                                        }
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})



@app.route('/elaboralagregar/' , methods = ['POST'])
def elaboral_agregar():
    if request.method == 'POST':
        compania = request.form.get('compania')
        funciones = request.form.get('funciones')
        contacto = request.form.get('contacto')
        posicion_contacto = request.form.get('posicion_contacto')
        telefono_contacto = request.form.get('telefono_contacto')
        telefono_contacto2 = request.form.get('telefono_contacto2')
        fecha_inicio = request.form.get('fecha_inicio')
        fecha_fin = request.form.get('fecha_fin')
        idcv = request.form.get('idcv')

          
        try:

            cv = BtExperienciaLaboral(
                compania=compania,
                funciones=funciones,
                nombre_contrato=contacto,
                posicion_contrato=posicion_contacto,
                telefono_contacto=telefono_contacto,
                telefono_contacto2=telefono_contacto2,
                fecha_inicio=fecha_inicio,
                fecha_fin=fecha_fin,
                id_persona=idcv
            )

            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_experiencia_laboral
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/elaboraleditar/' , methods = ['POST'])
def academicos_editar_id_post():
    if request.method == 'POST':
        compania = request.form.get('compania')
        funciones = request.form.get('funciones')
        contacto = request.form.get('contacto')
        posicion_contacto = request.form.get('posicion_contacto')
        telefono_contacto = request.form.get('telefono_contacto')
        telefono_contacto2 = request.form.get('telefono_contacto2')
        fecha_inicio = request.form.get('fecha_inicio')
        fecha_fin = request.form.get('fecha_fin')
        id = request.form.get('id')
         

        try:
            a = db.session.query(BtExperienciaLaboral).filter(BtExperienciaLaboral.id_experiencia_laboral==id).one()
            a.compania=compania
            a.funciones=funciones
            a.nombre_contrato=contacto
            a.posicion_contrato=posicion_contacto
            a.telefono_contacto=telefono_contacto
            a.telefono_contacto2=telefono_contacto2
            a.fecha_inicio=fecha_inicio
            a.fecha_fin=fecha_fin

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_persona,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/elaboraleliminar/' , methods = ['POST'])
def elaboral_id_eliminar():
    if request.method == 'POST':
        idc = request.form.get('id')
         
        try:
            a = db.session.query(BtExperienciaLaboral).filter(BtExperienciaLaboral.id_experiencia_laboral==idc).delete()
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

# idiomas por curriculum
@app.route('/idiomasall/<id>' , methods = ['GET'])
def lenguajes_id_all(id):
    if request.method == 'GET':

        try:
       
            u = db.session.query(BtCurriculumXLenguaje).filter(BtCurriculumXLenguaje.id_curriculum==id).all()
            
            a = list()
            for cv in u:
                i = db.session.query(BtLenguaje).filter(BtLenguaje.id_lenguaje==cv.id_lenguaje).one()
                a.append({
                    "id_curriculum_x_lenguaje" : cv.id_curriculum_x_lenguaje,
                        "escritura" : cv.escritura,
                    "lectura" : cv.lectura,
                    "conversacion" : cv.conversacion, 
                    "escucha" : cv.escucha,
                    "id_lenguaje" : cv.id_lenguaje,  
                    "id_curriculum" : cv.id_curriculum,
                    "lenguaje":i.lenguaje                        
                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})

#se obtienene todos los idiomas
@app.route('/lenguajeall/' , methods = ['GET'])
def lenguaje_all():
    if request.method == 'GET':

        try:
            u = db.session.query(BtLenguaje).all()
            
            a = list()
            for cv in u:
                a.append({
                    "id_lenguaje" : cv.id_lenguaje,
                    "lenguaje" : cv.lenguaje,
                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})

@app.route('/lenguajecvagregar/' , methods = ['POST'])
def lenguaje_x_persona_agregar():
    if request.method == 'POST':
        id_lenguaje = request.form.get('id_lenguaje')
        conversacion = request.form.get('conversacion')
        escritura = request.form.get('escritura')
        escucha = request.form.get('escucha')
        lectura = request.form.get('lectura')
    
        idcv = request.form.get('id')

          
        try:

            cv = BtCurriculumXLenguaje(
                id_lenguaje=id_lenguaje,
                conversacion=conversacion,
                escritura=escritura,
                escucha=escucha,
                lectura=lectura,
                id_curriculum=idcv            )

            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_curriculum_x_lenguaje
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/lenguajecvid/<id>' , methods = ['GET'])
def lenguajes_id_cv(id):
    if request.method == 'GET':

        try:
       
            cv = db.session.query(BtCurriculumXLenguaje).filter(BtCurriculumXLenguaje.id_curriculum_x_lenguaje==id).one()
            
            return jsonify(
                            {
                                "datos": {
                                        "id_curriculum_x_lenguaje" : cv.id_curriculum_x_lenguaje,
                                        "escritura" : cv.escritura,
                                        "lectura" : cv.lectura,
                                        "conversacion" : cv.conversacion, 
                                        "escucha" : cv.escucha,
                                        "id_lenguaje" : cv.id_lenguaje,  
                                        "id_curriculum" : cv.id_curriculum,                        
                                        }
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/lenguajecveditar/' , methods = ['POST'])
def lenguaje_x_persona_editar():
    if request.method == 'POST':
        id_lenguaje = request.form.get('id_lenguaje')
        conversacion = request.form.get('conversacion')
        escritura = request.form.get('escritura')
        escucha = request.form.get('escucha')
        lectura = request.form.get('lectura')
    
        idcv = request.form.get('id')

          
        try:
            a = db.session.query(BtCurriculumXLenguaje).filter(BtCurriculumXLenguaje.id_curriculum_x_lenguaje==idcv).one()
            a.id_lenguaje=id_lenguaje
            a.conversacion=conversacion
            a.escritura=escritura
            a.escucha=escucha
            a.lectura=lectura


            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_curriculum,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/lenguajecveliminar/' , methods = ['POST'])
def lenguajecv_id_eliminar():
    if request.method == 'POST':
        idc = request.form.get('id')
         
        try:
            a = db.session.query(BtCurriculumXLenguaje).filter(BtCurriculumXLenguaje.id_curriculum_x_lenguaje==idc).delete()
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })
#

# certificaciones por curriculum
@app.route('/certificacionesall/<id>' , methods = ['GET'])
def certificaciones_id_all(id):
    if request.method == 'GET':

        try:
            u = db.session.query(BdCertificacion).filter(BdCertificacion.id_persona==id).all()
            
            a = list()
            for cv in u:
                a.append({
                    "id_certificacion" : cv.id_certificacion,
                    "certificacion" : cv.certificacion,
                    "fecha_inicio" : cv.fecha_inicio,
                    "fecha_fin" : cv.fecha_fin, 
                    "tipo_certificacion" : cv.tipo_certificacion,
                    "codigo_certificacion" : cv.codigo_certificacion,  
                    "institucion" : cv.institucion, 
                
                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/certificacionagregar/' , methods = ['POST'])
def certificaciones_id_agregar():
    if request.method == 'POST':


        certificacion = request.form.get('certificacion')
        fecha_inicio = request.form.get('fecha_inicio')
        fecha_fin = request.form.get('fecha_fin')
        tipo_certificacion = request.form.get('tipo')
        codigo_certificacion = request.form.get('codigo')
        institucion = request.form.get('institucion')
        idcv = request.form.get('idcv')
         

        try:

            cv = BdCertificacion(
                certificacion=certificacion,
                fecha_inicio=fecha_inicio,
                fecha_fin=fecha_fin,
                tipo_certificacion=tipo_certificacion,
                codigo_certificacion=codigo_certificacion,
                institucion=institucion,
                id_persona=idcv
            )


            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_certificacion
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/certificacionesid/<id>' , methods = ['GET'])
def certificaciones_id(id):
    if request.method == 'GET':

        try:
            cv = db.session.query(BdCertificacion).filter(BdCertificacion.id_certificacion==id).one()
            
            return jsonify(
                            {
                                "datos": {
                                        "id_certificacion" : cv.id_certificacion,
                                        "certificacion" : cv.certificacion,
                                        "fecha_inicio" : cv.fecha_inicio,
                                        "fecha_fin" : cv.fecha_fin, 
                                        "tipo_certificacion" : cv.tipo_certificacion,
                                        "codigo_certificacion" : cv.codigo_certificacion,  
                                        "institucion" : cv.institucion, 
                                        "id_persona" : cv.id_persona,
                                        }
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/certificacioneditar/' , methods = ['POST'])
def certificaciones_id_editar():
    if request.method == 'POST':


        certificacion = request.form.get('certificacion')
        fecha_inicio = request.form.get('fecha_inicio')
        fecha_fin = request.form.get('fecha_fin')
        tipo_certificacion = request.form.get('tipo')
        codigo_certificacion = request.form.get('codigo')
        institucion = request.form.get('institucion')
        idc = request.form.get('idc')
         

        try:

            a = db.session.query(BdCertificacion).filter(BdCertificacion.id_certificacion==idc).one()

            a.certificacion=certificacion
            a.fecha_inicio=fecha_inicio
            a.fecha_fin=fecha_fin
            a.tipo_certificacion=tipo_certificacion,
            a.codigo_certificacion=codigo_certificacion  
            a.institucion=institucion, 

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_certificacion,
                    "id_persona" : a.id_persona,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/certificacioneliminar/' , methods = ['POST'])
def certificaciones_id_eliminar():
    if request.method == 'POST':

        idc = request.form.get('idc')
         

        try:

            a = db.session.query(BdCertificacion).filter(BdCertificacion.id_certificacion==idc).delete()

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

#Publicaciones
@app.route('/publicacionesall/<id>' , methods = ['GET'])
def publicaciones_id_all(id):
    if request.method == 'GET':

        try:
            u = db.session.query(BtPublicacion).filter(BtPublicacion.id_persona==id).all()
            
            a = list()
            for cv in u:
                a.append({
                    "id_publicacion" : cv.id_publicacion,
                    "publicacion" : cv.publicacion,
                    "lugar_publicacion" : cv.lugar_publicacion,
                    "fecha_publicacion" : cv.fecha_publicacion, 
                    "es_libro_edicion" : cv.es_libro_edicion,
                    "isbn" : cv.isbn,  
                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/publicaciongregar/' , methods = ['POST'])
def publicaciones_id_agregar():
    if request.method == 'POST':

        publicacion = request.form.get('publicacion')
        lugar = request.form.get('lugar')
        libro = request.form.get('libro')
        isbn = request.form.get('isbn')
        fecha_publicacion = request.form.get('fecha_publicacion')
        idcv = request.form.get('idcv')
         

        try:

            cv = BtPublicacion(
                publicacion=publicacion,
                lugar_publicacion=lugar,
                fecha_publicacion=fecha_publicacion,
                es_libro_edicion = libro,
                isbn=isbn,
                id_persona=idcv
            )


            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_publicacion
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/publicacionesid/<id>' , methods = ['GET'])
def publicaciones_id(id):
    if request.method == 'GET':

        try:
            cv = db.session.query(BtPublicacion).filter(BtPublicacion.id_publicacion==id).one()
            
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": {
                                    "id_publicacion" : cv.id_publicacion,
                                    "publicacion" : cv.publicacion,
                                    "lugar_publicacion" : cv.lugar_publicacion,
                                    "fecha_publicacion" : cv.fecha_publicacion, 
                                    "es_libro_edicion" : cv.es_libro_edicion,
                                    "isbn" : cv.isbn,
                                    "id_persona":cv.id_persona  
                                }
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/publicacioneditar/' , methods = ['POST'])
def certificaciones_modificar_id():
    if request.method == 'POST':


        publicacion = request.form.get('publicacion')
        lugar = request.form.get('lugar')
        libro = request.form.get('libro')
        isbn = request.form.get('isbn')
        fecha_publicacion = request.form.get('fecha_publicacion')
        id = request.form.get('id')
         

        try:

            a = db.session.query(BtPublicacion).filter(BtPublicacion.id_publicacion==id).one()

            a.publicacion=publicacion
            a.lugar_publicacion=lugar
            a.fecha_publicacion=fecha_publicacion
            a.es_libro_edicion=libro
            a.isbn=isbn

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_publicacion,
                    "id_persona" : a.id_persona,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/publicacioneliminar/' , methods = ['POST'])
def certificaciones_eliminar_id():
    if request.method == 'POST':

        id = request.form.get('id')
         
        try:

            a = db.session.query(BtPublicacion).filter(BtPublicacion.id_publicacion==id).delete()

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })



#Logros
@app.route('/logrosall/<id>' , methods = ['GET'])
def logros_id_all(id):
    if request.method == 'GET':

        #BtTipoLogro
        #BtCurriculumXLogro

        try:
            u = db.session.query(BtCurriculumXLogro).filter(BtCurriculumXLogro.id_curriculum==id).all()
            
            a = list()
            for cv in u:
                q = db.session.query(BtTipoLogro).filter(BtTipoLogro.id_tipo_logro==cv.id_tipo_logro).one()

                a.append({
                    "id_curriculum_x_logro" : cv.id_curriculum_x_logro,
                    "logro" : cv.logro,
                    "tipo_logro" : q.tipo_logro,
                    "id_tipo_logro" : cv.id_tipo_logro,
                    "fecha_inicio" : cv.fecha_inicio, 
                    "fecha_fin" : cv.fecha_fin,
                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/tipologro' , methods = ['GET'])
def tiposlogros_all():
    if request.method == 'GET':
        try:
            u = db.session.query(BtTipoLogro).all()
            
            a = list()
            for rol in u:
                a.append({ 
                    "id_tipo_logro" : rol.id_tipo_logro,
                    "tipo_logro" : rol.tipo_logro 
                    })
            
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a
                            }
                        )
        except:
            return jsonify({ "id_empresa_rol" : "0"})



@app.route('/logroagregar/' , methods = ['POST'])
def logros_id_agregar():
    if request.method == 'POST':

        logro = request.form.get('logro')
        tipo_logro = request.form.get('tipo_logro')
        fecha_inicio = request.form.get('fecha_inicio')
        fecha_fin = request.form.get('fecha_fin')
        idcv = request.form.get('idcv')
         

        try:

            cv = BtCurriculumXLogro(
                logro=logro,
                id_tipo_logro=tipo_logro,
                fecha_inicio=fecha_inicio,
                fecha_fin = fecha_fin,
                id_curriculum=idcv
            )


            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_curriculum_x_logro
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/logroid/<id>' , methods = ['GET'])
def logros_id(id):
    if request.method == 'GET':

        try:
            u = db.session.query(BtCurriculumXLogro).filter(BtCurriculumXLogro.id_curriculum_x_logro==id).one()
    
            #q = db.session.query(BtTipoLogro).filter(BtTipoLogro.id_tipo_logro==u.id_tipo_logro).one()

            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": {
                                        "id_curriculum_x_logro" : u.id_curriculum_x_logro,
                                        "logro" : u.logro,
                                        #"tipo_logro" : q.tipo_logro,
                                        "id_tipo_logro" : u.id_tipo_logro,
                                        "fecha_inicio" : u.fecha_inicio, 
                                        "fecha_fin" : u.fecha_fin,
                                        "id_curriculum" : u.id_curriculum,
                                        },
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})

@app.route('/logroeditar/' , methods = ['POST'])
def logro_modificar_id():
    if request.method == 'POST':


        logro = request.form.get('logro')
        tipo_logro = request.form.get('tipo_logro')
        fecha_inicio = request.form.get('fecha_inicio')
        fecha_fin = request.form.get('fecha_fin')
        
        id = request.form.get('id')
         

        try:

            a = db.session.query(BtCurriculumXLogro).filter(BtCurriculumXLogro.id_curriculum_x_logro==id).one()

            a.logro=logro
            a.id_tipo_logro=tipo_logro
            a.fecha_inicio=fecha_inicio
            a.fecha_fin=fecha_fin

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_curriculum_x_logro,
                    "id_persona" : a.id_curriculum,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/logroeliminar/' , methods = ['POST'])
def logro_eliminar_id():
    if request.method == 'POST':

        id = request.form.get('id')
         
        try:
            a = db.session.query(BtCurriculumXLogro).filter(BtCurriculumXLogro.id_curriculum_x_logro==id).delete()
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })




#RedesSociales
@app.route('/redsocialall/<id>' , methods = ['GET'])
def redsocal_id_all(id):
    if request.method == 'GET':


        try:
            u = db.session.query(BtCurriculumR).filter(BtCurriculumR.id_curriculum==id).all()
            
            a = list()
            for cv in u:
                q = db.session.query(BtRedSocial).filter(BtRedSocial.id_red_social==cv.id_red_social).one()

                a.append({
                    "id_curriculum_rs" : cv.id_curriculum_rs,
                    "red_social" : q.red_social
                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})

#RedesSociales
@app.route('/redsocialid/<id>' , methods = ['GET'])
def redsocal_id(id):
    if request.method == 'GET':


        try:
            u = db.session.query(BtCurriculumR).filter(BtCurriculumR.id_curriculum_rs==id).one()
            
            q = db.session.query(BtRedSocial).filter(BtRedSocial.id_red_social==u.id_red_social).one()

            return jsonify(
                            {
                                "datos": {
                                "id_curriculum_rs" : u.id_curriculum_rs,
                                "red_social" : q.red_social,
                                "id_curriculum" : u.id_curriculum
                                },
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/redsocialeliminar/' , methods = ['POST'])
def redsocial_eliminar_id():
    if request.method == 'POST':

        id = request.form.get('id')
         
        try:
            a = db.session.query(BtCurriculumR).filter(BtCurriculumR.id_curriculum_rs==id).delete()
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/rsall/' , methods = ['GET'])
def rs_all():
    if request.method == 'GET':

        try:
            u = db.session.query(BtRedSocial).all()
            
            a = list()
            for cv in u:
                a.append({
                    "id_red_social" : cv.id_red_social,
                    "red_social" : cv.red_social
                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})



@app.route('/redsocialagregar/' , methods = ['POST'])
def redsocial_id_agregar():
    if request.method == 'POST':
        redsocial = request.form.get('redsocial')
        id = request.form.get('id')
          
        try:

            cv = BtCurriculumR(
                id_curriculum=id,
                id_red_social=redsocial                
            )

            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_curriculum_rs
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })




#ExperienciaLaboral
@app.route('/experiencialaboralall/<id>' , methods = ['GET'])
def explaboral_id_all(id):
    if request.method == 'GET':

        try:
            u = db.session.query(BtExperienciaLaboral).filter(BtExperienciaLaboral.id_persona==id).all()
            
            a = list()
            for cv in u:
                a.append({
                    "id_experiencia_laboral" : cv.id_experiencia_laboral,
                    "compania" : cv.compania,
                    "funciones" : cv.funciones,
                    "nombre_contrato" : cv.nombre_contrato,
                    "posicion_contrato" : cv.posicion_contrato,
                    "telefono_contacto" : cv.telefono_contacto,
                    "telefono_contacto2" : cv.telefono_contacto2,
                    "fecha_inicio" : cv.fecha_inicio,
                    "fecha_fin" : cv.fecha_fin

                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


#Recomendacionlaboral
@app.route('/recomendacionlaboralall/<id>' , methods = ['GET'])
def reclaboral_id_all(id):
    if request.method == 'GET':

        try:
            u = db.session.query(BtRecomendacionLaboral).filter(BtRecomendacionLaboral.id_persona==id).all()
            
            a = list()
            for cv in u:
                a.append({
                    "id_recomendacion_laboral" : cv.id_recomendacion_laboral,
                    "nombre_contacto" : cv.nombre_contacto,
                    "institucion" : cv.institucion,
                    "telefono_contacto" : cv.telefono_contacto,
                    "telefono_contacto2" : cv.telefono_contacto2,

                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/reclaboralgregar/' , methods = ['POST'])
def reclaboral_agregar():
    if request.method == 'POST':
        contacto = request.form.get('nombre_contacto')
        institucion = request.form.get('institucion')
        telefono = request.form.get('telefono_contacto')
        telefono2 = request.form.get('telefono_contacto2')
        idcv = request.form.get('idcv')

          
        try:

            cv = BtRecomendacionLaboral(
                nombre_contacto=contacto,
                institucion=institucion,
                telefono_contacto=telefono,
                telefono_contacto2=telefono2,
                id_persona=idcv
            )

            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_recomendacion_laboral
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/reclaboralid/<id>' , methods = ['GET'])
def reclaboral_id(id):
    if request.method == 'GET':

        try:
            cv = db.session.query(BtRecomendacionLaboral).filter(BtRecomendacionLaboral.id_recomendacion_laboral==id).one()
            
            return jsonify(
                            {
                            "datos": {
                                   "id_recomendacion_laboral" : cv.id_recomendacion_laboral,
                                    "nombre_contacto" : cv.nombre_contacto,
                                    "institucion" : cv.institucion,
                                    "telefono_contacto" : cv.telefono_contacto,
                                    "telefono_contacto2" : cv.telefono_contacto2,
                                    "id_persona" : cv.id_persona
                                    },
                            "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/reclaboraleditar/' , methods = ['POST'])
def reclaboral_modificar_id():
    if request.method == 'POST':

        contacto = request.form.get('nombre_contacto')
        institucion = request.form.get('institucion')
        telefono = request.form.get('telefono_contacto')
        telefono2 = request.form.get('telefono_contacto2')
        idcv = request.form.get('idcv')
        

        try:

            a = db.session.query(BtRecomendacionLaboral).filter(BtRecomendacionLaboral.id_recomendacion_laboral==idcv).one()

            a.nombre_contacto=contacto
            a.institucion=institucion
            a.telefono_contacto=telefono
            a.telefono_contacto2=telefono2

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_recomendacion_laboral,
                    "id_persona" : a.id_persona,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/reclaboraleliminar/' , methods = ['POST'])
def recelaboral_eliminar_id():
    if request.method == 'POST':

        id = request.form.get('idcv')
         
        try:
            a = db.session.query(BtRecomendacionLaboral).filter(BtRecomendacionLaboral.id_recomendacion_laboral==id).delete()
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

#Recomendacionpersonal
@app.route('/recomendacionpersonalall/<id>' , methods = ['GET'])
def recpersonal_id_all(id):
    if request.method == 'GET':

        try:
            u = db.session.query(BtRecomendacionPersonal).filter(BtRecomendacionPersonal.id_persona==id).all()
            
            a = list()
            for cv in u:
                a.append({
                    "id_recomendacion_personal" : cv.id_recomendacion_personal,
                    "nombre_contacto" : cv.nombre_contacto,
                    "telefono_contacto" : cv.telefono_contacto,
                    "telefono_contacto2" : cv.telefono_contacto2,

                    })
            #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/recpersonalagregar/' , methods = ['POST'])
def recpersonal_agregar():
    if request.method == 'POST':
        contacto = request.form.get('nombre_contacto')
        telefono = request.form.get('telefono_contacto')
        telefono2 = request.form.get('telefono_contacto2')
        idcv = request.form.get('idcv')

          
        try:

            cv = BtRecomendacionPersonal(
                nombre_contacto=contacto,
                telefono_contacto=telefono,
                telefono_contacto2=telefono2,
                id_persona=idcv
            )

            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_recomendacion_personal
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/recpersonalid/<id>' , methods = ['GET'])
def recpersonal_id(id):
    if request.method == 'GET':

        try:
            cv = db.session.query(BtRecomendacionPersonal).filter(BtRecomendacionPersonal.id_recomendacion_personal==id).one()
            
            return jsonify(
                            {
                            "datos": {
                                   "id_recomendacion_personal" : cv.id_recomendacion_personal,
                                    "nombre_contacto" : cv.nombre_contacto,
                                    "telefono_contacto" : cv.telefono_contacto,
                                    "telefono_contacto2" : cv.telefono_contacto2,
                                    "id_persona" : cv.id_persona
                                    },
                            "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})

@app.route('/recpersonaleditar/' , methods = ['POST'])
def recpersonal_modificar_id():
    if request.method == 'POST':

        contacto = request.form.get('nombre_contacto')
        telefono = request.form.get('telefono_contacto')
        telefono2 = request.form.get('telefono_contacto2')
        idcv = request.form.get('idcv')
        

        try:

            a = db.session.query(BtRecomendacionPersonal).filter(BtRecomendacionPersonal.id_recomendacion_personal==idcv).one()
            a.nombre_contacto=contacto
            a.telefono_contacto=telefono
            a.telefono_contacto2=telefono2

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_recomendacion_personal,
                    "id_persona" : a.id_persona,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/recpersonaleliminar/' , methods = ['POST'])
def recpersonal_eliminar_id():
    if request.method == 'POST':

        id = request.form.get('idcv')
         
        try:
            a = db.session.query(BtRecomendacionPersonal).filter(BtRecomendacionPersonal.id_recomendacion_personal==id).delete()
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })



@app.route('/ofertasall/' , methods = ['GET'])
def ofertas_all():
    if request.method == 'GET':

        try:
            u = db.session.query(BtOfertaTrabajo).all()
            
            a = list()
            for cv in u:
                q = db.session.query(BtEstadoOferta).filter(BtEstadoOferta.id_estado_oferta==cv.id_estado_oferta).one()
                usuario = db.session.query(BtUsuario).filter(BtUsuario.id_usuario==cv.id_usuario).one()
                

                a.append({
                    "id_oferta_trabajo" : cv.id_oferta_trabajo,
                    "oferta_trabajo" : cv.oferta_trabajo,
                    "estado_oferta" : q.estado_oferta,
                    "salario_min" : str(cv.salario_min),
                    "salario_max" : str(cv.salario_max),
                    "id_municipio" : cv.id_municipio,
                    "descripcion_puesto" : cv.descripcion_puesto,
                    "experiencia_laboral" : cv.experiencia_laboral,
                    "usuario" : ("%s %s") % (usuario.nombres,usuario.apellidos),
                    "perfil_academico" : cv.perfil_academico,
                    "fecha_creacion" : cv.fecha_creacion,
                    })
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/oferta/<id>' , methods = ['GET'])
def ofertas_id(id):
    if request.method == 'GET':

        try:
            cv = db.session.query(BtOfertaTrabajo).filter(BtOfertaTrabajo.id_oferta_trabajo==id).one()
            

            q = db.session.query(BtEstadoOferta).filter(BtEstadoOferta.id_estado_oferta==cv.id_estado_oferta).one()
            usuario = db.session.query(BtUsuario).filter(BtUsuario.id_usuario==cv.id_usuario).one()
            municipio = db.session.query(BtMunicipio).filter(BtMunicipio.id_municipio==cv.id_municipio).one()
            departamento = db.session.query(BtDepartamento).filter(BtDepartamento.id_departamento==municipio.id_departamento).one()
                

            return jsonify(
                            {
                                "datos": {
                                        "id_oferta_trabajo" : cv.id_oferta_trabajo,
                                        "oferta_trabajo" : cv.oferta_trabajo,
                                        "estado_oferta" : q.estado_oferta,
                                        "salario_min" : str(cv.salario_min),
                                        "salario_max" : str(cv.salario_max),
                                        "municipio" : municipio.municipio,
                                        "departamento" : departamento.departamento,
                                        "descripcion_puesto" : cv.descripcion_puesto,
                                        "experiencia_laboral" : cv.experiencia_laboral,
                                        "usuario" : ("%s %s") % (usuario.nombres,usuario.apellidos),
                                        "perfil_academico" : cv.perfil_academico,
                                        "fecha_creacion" : cv.fecha_creacion,
                                        },
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})

@app.route('/ofertaagregar/' , methods = ['POST'])
def oferta_agregar():
    if request.method == 'POST':
        plaza = request.form.get('plaza')
        descripcion = request.form.get('descripcion')
        perfil_academico = request.form.get('perfil_academico')
        experiencia_laboral = request.form.get('experiencia_laboral')
        salario_minimo = request.form.get('salario_minimo')
        salario_maximo = request.form.get('salario_maximo')
        municipio = request.form.get('municipio')
          
        try:

            cv = BtOfertaTrabajo(
                oferta_trabajo=plaza,
                id_estado_oferta=1,
                salario_min=salario_minimo,
                salario_max=salario_maximo,
                experiencia_laboral=experiencia_laboral,
                perfil_academico=perfil_academico,
                id_empresa_usuario=1,
                id_usuario=1,
                id_municipio=1,
                descripcion_puesto=descripcion
            )

            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_oferta_trabajo
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/paisall/' , methods = ['GET'])
def pais_all():
    if request.method == 'GET':
        try:
            u = db.session.query(BtPai).all()            
            a = list()
            for cv in u:
                a.append({
                    "id_pais" : cv.id_pais,
                    "pais" : cv.pais,
                    "fecha_creacion" : cv.fecha_creacion,
                    })
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/departamento_all/<id>' , methods = ['GET'])
def deparamento_idpais(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BtDepartamento).filter(BtDepartamento.id_pais==id).all()           
            a = list()
            for cv in u:
                a.append({
                    "id_departamento" : cv.id_departamento,
                    "departamento" : cv.departamento,
                    "fecha_creacion" : cv.fecha_creacion,
                    })
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/municipio_all/<id>' , methods = ['GET'])
def municipio_iddepartamento(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BtDepartamento).filter(BtDepartamento.id_pais==id).all()           
            a = list()
            for cv in u:
                a.append({
                    "id_departamento" : cv.id_departamento,
                    "departamento" : cv.departamento,
                    "fecha_creacion" : cv.fecha_creacion,
                    })
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})



@app.route('/ofertaeditar/' , methods = ['POST'])
def oferta_editar_id():
    if request.method == 'POST':
        plaza = request.form.get('plaza')
        descripcion = request.form.get('descripcion')
        perfil_academico = request.form.get('perfil_academico')
        experiencia_laboral = request.form.get('experiencia_laboral')
        salario_minimo = request.form.get('salario_minimo')
        salario_maximo = request.form.get('salario_maximo')
        municipio = request.form.get('municipio')
        id = request.form.get('id')
         
       
        try:

            a = db.session.query(BtOfertaTrabajo).filter(BtOfertaTrabajo.id_oferta_trabajo==id).one()

            a.oferta_trabajo=plaza,
            a.salario_min=salario_minimo,
            a.salario_max=salario_maximo,
            a.experiencia_laboral=experiencia_laboral,
            a.perfil_academico=perfil_academico,
            a.descripcion_puesto=descripcion

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_oferta_trabajo,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

#Habilididades y destrezas de las plazas/ofertas
@app.route('/habdestrezasagregar/' , methods = ['POST'])
def habdestrezas_agregar():
    if request.method == 'POST':
        habilidad_destreza = request.form.get('habilidad_destreza')
        nivel = request.form.get('nivel')
        id = request.form.get('idcv')
          
        try:

            cv = BtHabilidadDestreza(
                habilidad_destreza=habilidad_destreza,
                nivel=nivel,
                id_oferta_tarabajo=id,
            )

            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_habilidad_destreza
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/habdestrezasall/<id>' , methods = ['GET'])
def habdestrezas_id_oferta(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BtHabilidadDestreza).filter(BtHabilidadDestreza.id_oferta_tarabajo==id).all()           
            a = list()
            for cv in u:
                a.append({
                    "id_habilidad_destreza" : cv.id_habilidad_destreza,
                    "habilidad_destrezas" : cv.habilidad_destreza,
                    "nivel" : cv.nivel,
                    })
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})

@app.route('/habdestrezasid/<id>' , methods = ['GET'])
def habdestrezas_id(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BtHabilidadDestreza).filter(BtHabilidadDestreza.id_habilidad_destreza==id).one()           
                
            return jsonify(
                            {
                                "datos": {
                                            "id_habilidad_destreza" : u.id_habilidad_destreza,
                                            "habilidad_destrezas" : u.habilidad_destreza,
                                            "nivel" : u.nivel,
                                            "id_oferta_tarabajo" : u.id_oferta_tarabajo
                                        },
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})

@app.route('/habdestrezaseditar/' , methods = ['POST'])
def habdestrezas_editar_id():
    if request.method == 'POST':
        habilidad_destreza = request.form.get('habilidad_destreza')
        nivel = request.form.get('nivel')
        id = request.form.get('idcv')
         
       
        try:

            a = db.session.query(BtHabilidadDestreza).filter(BtHabilidadDestreza.id_habilidad_destreza==id).one()

            a.habilidad_destreza=habilidad_destreza
            a.nivel=nivel

            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_oferta_tarabajo,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/habdestrezaseliminar/' , methods = ['POST'])
def habdestrezas_eliminar_id():
    if request.method == 'POST':
        id = request.form.get('idcv')
         
       
        try:
            a = db.session.query(BtHabilidadDestreza).filter(BtHabilidadDestreza.id_habilidad_destreza==id).delete()
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })


@app.route('/ofertaslaboralall/<id>' , methods = ['GET'])
def ofertalaboral_idcv(id):
    if request.method == 'GET':
        try:
            estado_oferta=1
            u = db.session.query(BtOfertaTrabajo).filter(BtOfertaTrabajo.id_estado_oferta==estado_oferta).all()           
            a = list()
            for cv in u:
                a.append({
                    "id_oferta_trabajo" : cv.id_oferta_trabajo,
                    "oferta_trabajo" : cv.oferta_trabajo,
                    "descripcion_puesto" : cv.descripcion_puesto,
                    })
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
            return jsonify({ "error" : 1 , "datos" : {}})

@app.route('/participacionagregar/' , methods = ['POST'])
def participacion_agregar():
    if request.method == 'POST':
        id_oferta_trabajo = request.form.get('id_oferta_trabajo')
        comentario = request.form.get('comentario')
        idCV = request.form.get('idcv')
          
        try:

            cv = BtCurriculumParticipacion(
                id_curriculum=idCV,
                id_oferta_trabajo=id_oferta_trabajo,
                comentarios=comentario,
            )

            db.session.add(cv)
            db.session.commit()            
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : cv.id_oferta_trabajo
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

#
@app.route('/participacionesall/<id>' , methods = ['GET'])
def participaciones_idcv(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BtCurriculumParticipacion).filter(BtCurriculumParticipacion.id_curriculum==id).all()           
            a = list()
            for cv in u:
                v = db.session.query(BtOfertaTrabajo).filter(BtOfertaTrabajo.id_oferta_trabajo==cv.id_oferta_trabajo).one()
                a.append({
                    "id_oferta_trabajo" : v.id_oferta_trabajo,
                    "oferta_trabajo" : v.oferta_trabajo,
                    "descripcion_puesto" : v.descripcion_puesto,
                    "id_participacion" : cv.id_curriculum_participacion,
                    "fecha_creacion" : str(cv.fecha_creacion),
                    "comentarios" : str(cv.comentarios),
                    })
            return jsonify(
                            {
                                "datos": a,
                                "estado":"ok"
                            }
                        )
        except:
           return jsonify({ "error" : 1 , "datos" : {}})


#
@app.route('/participacionid/<id>' , methods = ['GET'])
def participaciones_idparticipacion(id):
    if request.method == 'GET':
        try:
            u = db.session.query(BtCurriculumParticipacion).filter(BtCurriculumParticipacion.id_curriculum_participacion==id).one()           
            v = db.session.query(BtOfertaTrabajo).filter(BtOfertaTrabajo.id_oferta_trabajo==u.id_oferta_trabajo).one()
            return jsonify(
                            {
                                "datos": {
                                        "id_oferta_trabajo" : v.id_oferta_trabajo,
                                        "oferta_trabajo" : v.oferta_trabajo,
                                        "descripcion_puesto" : v.descripcion_puesto,
                                        "id_participacion" : u.id_curriculum_participacion,
                                        "fecha_creacion" : str(u.fecha_creacion),
                                        "comentarios" : str(u.comentarios),
                                        "id_curriculum" : u.id_curriculum,
                                        },
                                "estado":"ok"
                            }
                        )
        except:
           return jsonify({ "error" : 1 , "datos" : {}})


@app.route('/participacioneditar/' , methods = ['POST'])
def participacion_editar_id():
    if request.method == 'POST':
        comentarios = request.form.get('comentario')
        id = request.form.get('id')
         
        try:
            a = db.session.query(BtCurriculumParticipacion).filter(BtCurriculumParticipacion.id_curriculum_participacion==id).one()
            a.comentarios=comentarios
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "id" : a.id_curriculum,
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

@app.route('/participacioneliminar/' , methods = ['POST'])
def participacion_eliminar_id():
    if request.method == 'POST':
        id = request.form.get('id')
         
        try:
            a = db.session.query(BtCurriculumParticipacion).filter(BtCurriculumParticipacion.id_curriculum_participacion==id).delete()
            db.session.commit()
            return jsonify({
                "estado" : "ok",
                "datos" : {
                    "estado":"ok"
                    }})
        except:
            return jsonify({ "error" : 1, "datos" : {} })

#run flask app
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0', port=3000)

