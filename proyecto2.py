# coding: utf-8
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()



t_audit_persona = db.Table(
    'audit_persona',
    db.Column('id_persona', db.Integer, nullable=False, server_default=db.FetchedValue()),
    db.Column('id_persona_estado', db.Integer, nullable=False),
    db.Column('nombres', db.String(250), nullable=False),
    db.Column('apellidos', db.String(250), nullable=False),
    db.Column('genero', db.String(2), nullable=False),
    db.Column('fecha_nacimiento', db.Date, nullable=False),
    db.Column('dui_pasaporte', db.String(50), nullable=False),
    db.Column('nit', db.String(17), nullable=False),
    db.Column('nup', db.String(50), nullable=False),
    db.Column('direccion', db.String(250), nullable=False),
    db.Column('correo_electronico', db.String(100), nullable=False),
    db.Column('fecha_creacion', db.DateTime(True), nullable=False, server_default=db.FetchedValue())
)



class AuditUsuariosBloqueado(db.Model):
    __tablename__ = 'audit_usuarios_bloqueados'

    id_audit_usuarios_bloqueados = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_usuario = db.Column(db.BigInteger, nullable=False)
    usuario = db.Column(db.String(250), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BdCertificacion(db.Model):
    __tablename__ = 'bd_certificacion'

    id_certificacion = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    certificacion = db.Column(db.String(200), nullable=False)
    fecha_inicio = db.Column(db.Date, nullable=False)
    fecha_fin = db.Column(db.Date, nullable=False)
    tipo_certificacion = db.Column(db.String(100), nullable=False)
    codigo_certificacion = db.Column(db.String(25), nullable=False)
    institucion = db.Column(db.String(100), nullable=False)
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BdCertificacion.id_persona == BtPersona.id_persona', backref='bd_certificacions')



class BdConocimientoAcademico(db.Model):
    __tablename__ = 'bd_conocimiento_academico'

    id_conocimiento_academico = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    titulo = db.Column(db.String(250), nullable=False)
    institucion = db.Column(db.String(250), nullable=False)
    fecha_inicio = db.Column(db.Date, nullable=False)
    fecha_fin = db.Column(db.Date)
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BdConocimientoAcademico.id_persona == BtPersona.id_persona', backref='bd_conocimiento_academicoes')



class BdPersonaTelefono(db.Model):
    __tablename__ = 'bd_persona_telefono'

    id_persona_telefono = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    tipo = db.Column(db.String(50), nullable=False)
    telefono = db.Column(db.String(25), nullable=False)
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BdPersonaTelefono.id_persona == BtPersona.id_persona', backref='bd_persona_telefonoes')



class BtDepartamento(db.Model):
    __tablename__ = 'bt_departamento'

    id_departamento = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    departamento = db.Column(db.String(50), nullable=False)
    id_pais = db.Column(db.ForeignKey('bt_pais.id_pais', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_pai = db.relationship('BtPai', primaryjoin='BtDepartamento.id_pais == BtPai.id_pais', backref='bt_departamentoes')



class BtEmpresa(db.Model):
    __tablename__ = 'bt_empresa'

    id_empresa = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    empresa = db.Column(db.String(100), nullable=False)
    razon_social = db.Column(db.String(100))
    direccion = db.Column(db.String(200), nullable=False)
    telefono_1 = db.Column(db.String(15), nullable=False)
    codigo_empresa = db.Column(db.String(20), nullable=False)
    giro = db.Column(db.String(50), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtEmpresaRol(db.Model):
    __tablename__ = 'bt_empresa_rol'

    id_empresa_rol = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    rol = db.Column(db.String(50), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtEmpresaUsuario(db.Model):
    __tablename__ = 'bt_empresa_usuario'

    id_empresa_usuario = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    nombres = db.Column(db.String(200), nullable=False)
    apellidos = db.Column(db.String(200), nullable=False)
    id_empresa_rol = db.Column(db.ForeignKey('bt_empresa_rol.id_empresa_rol', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    password = db.Column(db.String(500), nullable=False)
    correo_electronico = db.Column(db.String(50), nullable=False)
    id_estado_empresa_usuario = db.Column(db.ForeignKey('bt_estado_empresa_usuario.id_estado_empresa_usuario', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_empresa = db.Column(db.ForeignKey('bt_empresa.id_empresa', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_empresa = db.relationship('BtEmpresa', primaryjoin='BtEmpresaUsuario.id_empresa == BtEmpresa.id_empresa', backref='bt_empresa_usuarios')
    bt_empresa_rol = db.relationship('BtEmpresaRol', primaryjoin='BtEmpresaUsuario.id_empresa_rol == BtEmpresaRol.id_empresa_rol', backref='bt_empresa_usuarios')
    bt_estado_empresa_usuario = db.relationship('BtEstadoEmpresaUsuario', primaryjoin='BtEmpresaUsuario.id_estado_empresa_usuario == BtEstadoEmpresaUsuario.id_estado_empresa_usuario', backref='bt_empresa_usuarios')



class BtEstadoEmpresaUsuario(db.Model):
    __tablename__ = 'bt_estado_empresa_usuario'

    id_estado_empresa_usuario = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    estado_empresa_usuario = db.Column(db.String(50), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtEstadoOferta(db.Model):
    __tablename__ = 'bt_estado_oferta'

    id_estado_oferta = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    estado_oferta = db.Column(db.String(100), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtEstadoParticipacion(db.Model):
    __tablename__ = 'bt_estado_participacion'

    id_estado_participacion = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    estado_persona_partipacion = db.Column(db.String(50), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtEstadoPrueba(db.Model):
    __tablename__ = 'bt_estado_prueba'

    id_estado_prueba = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    estado_prueba = db.Column(db.String(50), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtEstadoUsuario(db.Model):
    __tablename__ = 'bt_estado_usuario'

    id_estado_usuario = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    estado_usuario = db.Column(db.String(50), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtExperienciaLaboral(db.Model):
    __tablename__ = 'bt_experiencia_laboral'

    id_experiencia_laboral = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    compania = db.Column(db.String(150), nullable=False)
    funciones = db.Column(db.Text, nullable=False)
    nombre_contrato = db.Column(db.String(250), nullable=False)
    posicion_contrato = db.Column(db.String(150), nullable=False)
    telefono_contacto = db.Column(db.String(20), nullable=False)
    telefono_contacto2 = db.Column(db.String(20))
    fecha_inicio = db.Column(db.Date, nullable=False)
    fecha_fin = db.Column(db.Date)
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BtExperienciaLaboral.id_persona == BtPersona.id_persona', backref='bt_experiencia_laborals')



class BtHabilidadDestreza(db.Model):
    __tablename__ = 'bt_habilidad_destreza'

    id_habilidad_destreza = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_oferta_trabajo = db.Column(db.ForeignKey('bt_oferta_trabajo.id_oferta_trabajo', ondelete='RESTRICT', onupdate='RESTRICT'))
    habilidad_destreza = db.Column(db.String(200), nullable=False)
    id_oferta_tarabajo = db.Column(db.BigInteger, nullable=False)
    nivel = db.Column(db.BigInteger, nullable=False)
    fecha_nacimiento = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_oferta_trabajo = db.relationship('BtOfertaTrabajo', primaryjoin='BtHabilidadDestreza.id_oferta_trabajo == BtOfertaTrabajo.id_oferta_trabajo', backref='bt_habilidad_destrezas')



class BtHabilidadTecnica(db.Model):
    __tablename__ = 'bt_habilidad_tecnica'

    id_habilidad_tecnica = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona_x_ht = db.Column(db.ForeignKey('bt_persona_x_ht.id_persona_x_ht', ondelete='RESTRICT', onupdate='RESTRICT'))
    habilidad = db.Column(db.String(250), nullable=False)
    id_naturaleza_ht = db.Column(db.BigInteger, nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona_x_ht = db.relationship('BtPersonaXHt', primaryjoin='BtHabilidadTecnica.id_persona_x_ht == BtPersonaXHt.id_persona_x_ht', backref='bt_habilidad_tecnicas')



class BtLenguaje(db.Model):
    __tablename__ = 'bt_lenguaje'

    id_lenguaje = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona_x_lenguaje = db.Column(db.ForeignKey('bt_persona_x_lenguaje.id_persona_x_lenguaje', ondelete='RESTRICT', onupdate='RESTRICT'))
    lenguaje = db.Column(db.String(100), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona_x_lenguaje = db.relationship('BtPersonaXLenguaje', primaryjoin='BtLenguaje.id_persona_x_lenguaje == BtPersonaXLenguaje.id_persona_x_lenguaje', backref='bt_lenguajes')



class BtMunicipio(db.Model):
    __tablename__ = 'bt_municipio'

    id_municipio = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    municipio = db.Column(db.String(50), nullable=False)
    id_departamento = db.Column(db.ForeignKey('bt_departamento.id_departamento', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_departamento = db.relationship('BtDepartamento', primaryjoin='BtMunicipio.id_departamento == BtDepartamento.id_departamento', backref='bt_municipios')



class BtNaturalezaHt(db.Model):
    __tablename__ = 'bt_naturaleza_ht'

    id_naturaleza_ht = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_habilidad_tecnica = db.Column(db.ForeignKey('bt_habilidad_tecnica.id_habilidad_tecnica', ondelete='RESTRICT', onupdate='RESTRICT'))
    naturaleza_tecnica = db.Column(db.String(250), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_habilidad_tecnica = db.relationship('BtHabilidadTecnica', primaryjoin='BtNaturalezaHt.id_habilidad_tecnica == BtHabilidadTecnica.id_habilidad_tecnica', backref='bt_naturaleza_hts')



class BtOfertaConocimiento(db.Model):
    __tablename__ = 'bt_oferta_conocimiento'

    id_oferta_conocimiento = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    conocimiento = db.Column(db.String(500), nullable=False)
    nivel = db.Column(db.BigInteger, nullable=False)
    id_oferta_trabajo = db.Column(db.ForeignKey('bt_oferta_trabajo.id_oferta_trabajo', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_oferta_trabajo = db.relationship('BtOfertaTrabajo', primaryjoin='BtOfertaConocimiento.id_oferta_trabajo == BtOfertaTrabajo.id_oferta_trabajo', backref='bt_oferta_conocimientoes')



class BtOfertaTrabajo(db.Model):
    __tablename__ = 'bt_oferta_trabajo'

    id_oferta_trabajo = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    oferta_trabajo = db.Column(db.String(200))
    id_estado_oferta = db.Column(db.ForeignKey('bt_estado_oferta.id_estado_oferta', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    salario_min = db.Column(db.Numeric(4, 2), nullable=False)
    salario_max = db.Column(db.Numeric(4, 2), nullable=False)
    id_municipio = db.Column(db.ForeignKey('bt_municipio.id_municipio', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    descripcion_puesto = db.Column(db.Text, nullable=False)
    experiencia_laboral = db.Column(db.Text, nullable=False)
    id_usuario = db.Column(db.ForeignKey('bt_usuario.id_usuario', ondelete='RESTRICT', onupdate='RESTRICT'))
    perfil_academico = db.Column(db.String(100), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())
    id_empresa_usuario = db.Column(db.ForeignKey('bt_empresa_usuario.id_empresa_usuario', ondelete='RESTRICT', onupdate='RESTRICT'))

    bt_empresa_usuario = db.relationship('BtEmpresaUsuario', primaryjoin='BtOfertaTrabajo.id_empresa_usuario == BtEmpresaUsuario.id_empresa_usuario', backref='bt_oferta_trabajoes')
    bt_estado_oferta = db.relationship('BtEstadoOferta', primaryjoin='BtOfertaTrabajo.id_estado_oferta == BtEstadoOferta.id_estado_oferta', backref='bt_oferta_trabajoes')
    bt_municipio = db.relationship('BtMunicipio', primaryjoin='BtOfertaTrabajo.id_municipio == BtMunicipio.id_municipio', backref='bt_oferta_trabajoes')
    bt_usuario = db.relationship('BtUsuario', primaryjoin='BtOfertaTrabajo.id_usuario == BtUsuario.id_usuario', backref='bt_oferta_trabajoes')



class BtPai(db.Model):
    __tablename__ = 'bt_pais'

    id_pais = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    pais = db.Column(db.String(200), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtParticipacionEvento(db.Model):
    __tablename__ = 'bt_participacion_evento'

    id_participacion_evento = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    evento = db.Column(db.String(200), nullable=False)
    anfitrion = db.Column(db.String(200), nullable=False)
    fecha_inicio = db.Column(db.Date, nullable=False)
    fecha_fin = db.Column(db.Date, nullable=False)
    lugar = db.Column(db.String(150), nullable=False)
    id_pais = db.Column(db.ForeignKey('bt_pais.id_pais', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_pai = db.relationship('BtPai', primaryjoin='BtParticipacionEvento.id_pais == BtPai.id_pais', backref='bt_participacion_eventoes')
    bt_persona = db.relationship('BtPersona', primaryjoin='BtParticipacionEvento.id_persona == BtPersona.id_persona', backref='bt_participacion_eventoes')



class BtPersona(db.Model):
    __tablename__ = 'bt_persona'

    id_persona = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona_estado = db.Column(db.ForeignKey('bt_persona_esado.id_persona_estado', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    nombres = db.Column(db.String(250), nullable=False)
    apellidos = db.Column(db.String(250), nullable=False)
    genero = db.Column(db.String(2), nullable=False)
    fecha_nacimiento = db.Column(db.Date, nullable=False)
    dui_pasaporte = db.Column(db.String(50), nullable=False)
    nit = db.Column(db.String(17), nullable=False)
    nup = db.Column(db.String(50), nullable=False)
    direccion = db.Column(db.String(250), nullable=False)
    correo_electronico = db.Column(db.String(100), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona_esado = db.relationship('BtPersonaEsado', primaryjoin='BtPersona.id_persona_estado == BtPersonaEsado.id_persona_estado', backref='bt_personae')



class BtPersonaEsado(db.Model):
    __tablename__ = 'bt_persona_esado'

    id_persona_estado = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    estado_persona = db.Column(db.String(200), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtPersonaPa(db.Model):
    __tablename__ = 'bt_persona_pa'

    id_persona_pa = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_prueba_actitud = db.Column(db.BigInteger, nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BtPersonaPa.id_persona == BtPersona.id_persona', backref='bt_persona_pas')



class BtPersonaParticipacion(db.Model):
    __tablename__ = 'bt_persona_participacion'

    id_persona_participacion = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_oferta_trabajo = db.Column(db.ForeignKey('bt_oferta_trabajo.id_oferta_trabajo', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_estado_participacion = db.Column(db.ForeignKey('bt_estado_participacion.id_estado_participacion', ondelete='RESTRICT', onupdate='RESTRICT'))
    comentarios = db.Column(db.Text)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False)

    bt_estado_participacion = db.relationship('BtEstadoParticipacion', primaryjoin='BtPersonaParticipacion.id_estado_participacion == BtEstadoParticipacion.id_estado_participacion', backref='bt_persona_participacions')
    bt_oferta_trabajo = db.relationship('BtOfertaTrabajo', primaryjoin='BtPersonaParticipacion.id_oferta_trabajo == BtOfertaTrabajo.id_oferta_trabajo', backref='bt_persona_participacions')
    bt_persona = db.relationship('BtPersona', primaryjoin='BtPersonaParticipacion.id_persona == BtPersona.id_persona', backref='bt_persona_participacions')



class BtPersonaPc(db.Model):
    __tablename__ = 'bt_persona_pc'

    id_persona_pc = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_prueba_conocimiento = db.Column(db.ForeignKey('bt_prueba_conocimiento.id_prueba_conocimiento', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BtPersonaPc.id_persona == BtPersona.id_persona', backref='bt_persona_pcs')
    bt_prueba_conocimiento = db.relationship('BtPruebaConocimiento', primaryjoin='BtPersonaPc.id_prueba_conocimiento == BtPruebaConocimiento.id_prueba_conocimiento', backref='bt_persona_pcs')



class BtPersonaP(db.Model):
    __tablename__ = 'bt_persona_ps'

    id_persona_ps = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_prueba_psicologica = db.Column(db.BigInteger, nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BtPersonaP.id_persona == BtPersona.id_persona', backref='bt_persona_ps')



class BtPersonaR(db.Model):
    __tablename__ = 'bt_persona_rs'

    id_persona_rs = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_red_social = db.Column(db.ForeignKey('bt_red_social.id_red_social', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BtPersonaR.id_persona == BtPersona.id_persona', backref='bt_persona_rs')
    bt_red_social = db.relationship('BtRedSocial', primaryjoin='BtPersonaR.id_red_social == BtRedSocial.id_red_social', backref='bt_persona_rs')



class BtPersonaXHt(db.Model):
    __tablename__ = 'bt_persona_x_ht'

    id_persona_x_ht = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_conocimiento_academico = db.Column(db.ForeignKey('bd_conocimiento_academico.id_conocimiento_academico', ondelete='RESTRICT', onupdate='RESTRICT'))
    id_habilidad_tecnica = db.Column(db.BigInteger, nullable=False)
    id_persona = db.Column(db.BigInteger, nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bd_conocimiento_academico = db.relationship('BdConocimientoAcademico', primaryjoin='BtPersonaXHt.id_conocimiento_academico == BdConocimientoAcademico.id_conocimiento_academico', backref='bt_persona_x_hts')



class BtPersonaXLenguaje(db.Model):
    __tablename__ = 'bt_persona_x_lenguaje'

    id_persona_x_lenguaje = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_conocimiento_academico = db.Column(db.ForeignKey('bd_conocimiento_academico.id_conocimiento_academico', ondelete='RESTRICT', onupdate='RESTRICT'))
    escritura = db.Column(db.String(2), nullable=False)
    lectura = db.Column(db.String(2), nullable=False)
    conversacion = db.Column(db.String(2), nullable=False)
    escucha = db.Column(db.String(2), nullable=False)
    id_lenguaje = db.Column(db.BigInteger, nullable=False)
    id_persona = db.Column(db.BigInteger, nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bd_conocimiento_academico = db.relationship('BdConocimientoAcademico', primaryjoin='BtPersonaXLenguaje.id_conocimiento_academico == BdConocimientoAcademico.id_conocimiento_academico', backref='bt_persona_x_lenguajes')



class BtPersonaXLogro(db.Model):
    __tablename__ = 'bt_persona_x_logro'

    id_persona_x_logro = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_conocimiento_academico = db.Column(db.ForeignKey('bd_conocimiento_academico.id_conocimiento_academico', ondelete='RESTRICT', onupdate='RESTRICT'))
    logro = db.Column(db.String(200), nullable=False)
    id_tipo_logro = db.Column(db.BigInteger, nullable=False)
    fecha_inicio = db.Column(db.Date, nullable=False)
    fecha_fin = db.Column(db.Date)
    id_persona = db.Column(db.BigInteger, nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bd_conocimiento_academico = db.relationship('BdConocimientoAcademico', primaryjoin='BtPersonaXLogro.id_conocimiento_academico == BdConocimientoAcademico.id_conocimiento_academico', backref='bt_persona_x_logroes')



class BtPersonasPaResp(db.Model):
    __tablename__ = 'bt_personas_pa_resp'

    id_personas_pa_resp = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona_pa = db.Column(db.ForeignKey('bt_persona_pa.id_persona_pa', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_resp_pa = db.Column(db.ForeignKey('bt_resp_pa.id_resp_pa', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona_pa = db.relationship('BtPersonaPa', primaryjoin='BtPersonasPaResp.id_persona_pa == BtPersonaPa.id_persona_pa', backref='bt_personas_pa_resps')
    bt_resp_pa = db.relationship('BtRespPa', primaryjoin='BtPersonasPaResp.id_resp_pa == BtRespPa.id_resp_pa', backref='bt_personas_pa_resps')



class BtPersonasPcResp(db.Model):
    __tablename__ = 'bt_personas_pc_resp'

    id_personas_pc_resp = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona_pc = db.Column(db.ForeignKey('bt_persona_pc.id_persona_pc', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_resp_pc_om = db.Column(db.ForeignKey('bt_resp_pc_om.id_resp_pc_om', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona_pc = db.relationship('BtPersonaPc', primaryjoin='BtPersonasPcResp.id_persona_pc == BtPersonaPc.id_persona_pc', backref='bt_personas_pc_resps')
    bt_resp_pc_om = db.relationship('BtRespPcOm', primaryjoin='BtPersonasPcResp.id_resp_pc_om == BtRespPcOm.id_resp_pc_om', backref='bt_personas_pc_resps')



class BtPersonasPsResp(db.Model):
    __tablename__ = 'bt_personas_ps_resp'

    id_personas_ps_resp = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona_ps = db.Column(db.ForeignKey('bt_persona_ps.id_persona_ps', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_resp_ps = db.Column(db.ForeignKey('bt_resp_ps.id_resp_ps', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona_p = db.relationship('BtPersonaP', primaryjoin='BtPersonasPsResp.id_persona_ps == BtPersonaP.id_persona_ps', backref='bt_personas_ps_resps')
    bt_resp_p = db.relationship('BtRespP', primaryjoin='BtPersonasPsResp.id_resp_ps == BtRespP.id_resp_ps', backref='bt_personas_ps_resps')



class BtPpConocimiento(db.Model):
    __tablename__ = 'bt_pp_conocimientos'

    id_pp_conocimientos = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona_participacion = db.Column(db.ForeignKey('bt_persona_participacion.id_persona_participacion', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_oferta_conocimiento = db.Column(db.ForeignKey('bt_oferta_conocimiento.id_oferta_conocimiento', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    comentario = db.Column(db.String(200), nullable=False)
    fecha_conocimiento = db.Column(db.DateTime(True), nullable=False)

    bt_oferta_conocimiento = db.relationship('BtOfertaConocimiento', primaryjoin='BtPpConocimiento.id_oferta_conocimiento == BtOfertaConocimiento.id_oferta_conocimiento', backref='bt_pp_conocimientoes')
    bt_persona_participacion = db.relationship('BtPersonaParticipacion', primaryjoin='BtPpConocimiento.id_persona_participacion == BtPersonaParticipacion.id_persona_participacion', backref='bt_pp_conocimientoes')



class BtPpHabilidad(db.Model):
    __tablename__ = 'bt_pp_habilidad'

    id_pp_habilidad = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona_participacion = db.Column(db.ForeignKey('bt_persona_participacion.id_persona_participacion', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_habilidad_destreza = db.Column(db.ForeignKey('bt_habilidad_destreza.id_habilidad_destreza', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    comentario = db.Column(db.String(200), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False)

    bt_habilidad_destreza = db.relationship('BtHabilidadDestreza', primaryjoin='BtPpHabilidad.id_habilidad_destreza == BtHabilidadDestreza.id_habilidad_destreza', backref='bt_pp_habilidads')
    bt_persona_participacion = db.relationship('BtPersonaParticipacion', primaryjoin='BtPpHabilidad.id_persona_participacion == BtPersonaParticipacion.id_persona_participacion', backref='bt_pp_habilidads')



class BtPregPa(db.Model):
    __tablename__ = 'bt_preg_pa'

    id_preg_pa = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    pregunta = db.Column(db.String(500), nullable=False)
    comentario = db.Column(db.String(500), nullable=False)
    id_prueba_actitud = db.Column(db.ForeignKey('bt_prueba_actitud.id_prueba_actitud', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_prueba_actitud = db.relationship('BtPruebaActitud', primaryjoin='BtPregPa.id_prueba_actitud == BtPruebaActitud.id_prueba_actitud', backref='bt_preg_pas')



class BtPregPc(db.Model):
    __tablename__ = 'bt_preg_pc'

    id_preg_pc = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    pregunta = db.Column(db.String(500), nullable=False)
    comentario = db.Column(db.String(500), nullable=False)
    id_prueba_conocimiento = db.Column(db.ForeignKey('bt_prueba_conocimiento.id_prueba_conocimiento', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_prueba_conocimiento = db.relationship('BtPruebaConocimiento', primaryjoin='BtPregPc.id_prueba_conocimiento == BtPruebaConocimiento.id_prueba_conocimiento', backref='bt_preg_pcs')



class BtPregP(db.Model):
    __tablename__ = 'bt_preg_ps'

    id_preg_ps = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    pregunta = db.Column(db.String(500), nullable=False)
    comentario = db.Column(db.String(500), nullable=False)
    id_prueba_psicologica = db.Column(db.ForeignKey('bt_prueba_psicologica.id_prueba_psicologica', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_prueba_psicologica = db.relationship('BtPruebaPsicologica', primaryjoin='BtPregP.id_prueba_psicologica == BtPruebaPsicologica.id_prueba_psicologica', backref='bt_preg_ps')



class BtPrivilegioEmpresa(db.Model):
    __tablename__ = 'bt_privilegio_empresa'

    id_privilegio_empresa = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    menu_seccion = db.Column(db.String(50), nullable=False)
    crear = db.Column(db.String(2), nullable=False)
    modificar = db.Column(db.String(2), nullable=False)
    eliminar = db.Column(db.String(2), nullable=False)
    ver = db.Column(db.String(2), nullable=False)
    id_empresa_rol = db.Column(db.ForeignKey('bt_empresa_rol.id_empresa_rol', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_empresa_rol = db.relationship('BtEmpresaRol', primaryjoin='BtPrivilegioEmpresa.id_empresa_rol == BtEmpresaRol.id_empresa_rol', backref='bt_privilegio_empresas')



class BtPrivilegioUsuario(db.Model):
    __tablename__ = 'bt_privilegio_usuario'

    id_privilegio_usuario = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    menu_seccion = db.Column(db.String(50), nullable=False)
    crear = db.Column(db.String(2), nullable=False)
    modificar = db.Column(db.String(2), nullable=False)
    eliminar = db.Column(db.String(2), nullable=False)
    ver = db.Column(db.String(2), nullable=False)
    id_rol = db.Column(db.ForeignKey('bt_rol.id_rol', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_rol = db.relationship('BtRol', primaryjoin='BtPrivilegioUsuario.id_rol == BtRol.id_rol', backref='bt_privilegio_usuarios')



class BtPruebaActitud(db.Model):
    __tablename__ = 'bt_prueba_actitud'

    id_prueba_actitud = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    nombre = db.Column(db.String(250), nullable=False)
    id_estado_prueba = db.Column(db.ForeignKey('bt_estado_prueba.id_estado_prueba', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    descripcion = db.Column(db.String(500), nullable=False)
    id_usuario = db.Column(db.ForeignKey('bt_usuario.id_usuario', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_estado_prueba = db.relationship('BtEstadoPrueba', primaryjoin='BtPruebaActitud.id_estado_prueba == BtEstadoPrueba.id_estado_prueba', backref='bt_prueba_actituds')
    bt_usuario = db.relationship('BtUsuario', primaryjoin='BtPruebaActitud.id_usuario == BtUsuario.id_usuario', backref='bt_prueba_actituds')



class BtPruebaConocimiento(db.Model):
    __tablename__ = 'bt_prueba_conocimiento'

    id_prueba_conocimiento = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    nombre = db.Column(db.String(250), nullable=False)
    id_estado_prueba = db.Column(db.ForeignKey('bt_estado_prueba.id_estado_prueba', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    descripcion = db.Column(db.String(500), nullable=False)
    id_usuario = db.Column(db.ForeignKey('bt_usuario.id_usuario', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_estado_prueba = db.relationship('BtEstadoPrueba', primaryjoin='BtPruebaConocimiento.id_estado_prueba == BtEstadoPrueba.id_estado_prueba', backref='bt_prueba_conocimientoes')
    bt_usuario = db.relationship('BtUsuario', primaryjoin='BtPruebaConocimiento.id_usuario == BtUsuario.id_usuario', backref='bt_prueba_conocimientoes')



class BtPruebaPsicologica(db.Model):
    __tablename__ = 'bt_prueba_psicologica'

    id_prueba_psicologica = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    nombre = db.Column(db.String(250), nullable=False)
    id_estado_prueba = db.Column(db.ForeignKey('bt_estado_prueba.id_estado_prueba', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    descripcion = db.Column(db.String(500), nullable=False)
    id_usuario = db.Column(db.ForeignKey('bt_usuario.id_usuario', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_estado_prueba = db.relationship('BtEstadoPrueba', primaryjoin='BtPruebaPsicologica.id_estado_prueba == BtEstadoPrueba.id_estado_prueba', backref='bt_prueba_psicologicas')
    bt_usuario = db.relationship('BtUsuario', primaryjoin='BtPruebaPsicologica.id_usuario == BtUsuario.id_usuario', backref='bt_prueba_psicologicas')



class BtPublicacion(db.Model):
    __tablename__ = 'bt_publicacion'

    id_publicacion = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    publicacion = db.Column(db.String(200), nullable=False)
    lugar_publicacion = db.Column(db.String(200), nullable=False)
    fecha_publicacion = db.Column(db.Date, nullable=False)
    es_libro_edicion = db.Column(db.String(25))
    isbn = db.Column(db.String(25))
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_persona = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BtPublicacion.id_persona == BtPersona.id_persona', backref='bt_publicacions')



class BtRecomendacionLaboral(db.Model):
    __tablename__ = 'bt_recomendacion_laboral'

    id_recomendacion_laboral = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    nombre_contacto = db.Column(db.String(200), nullable=False)
    institucion = db.Column(db.String(200), nullable=False)
    telefono_contacto = db.Column(db.String(20), nullable=False)
    telefono_contacto2 = db.Column(db.String(20))
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BtRecomendacionLaboral.id_persona == BtPersona.id_persona', backref='bt_recomendacion_laborals')



class BtRecomendacionPersonal(db.Model):
    __tablename__ = 'bt_recomendacion_personal'

    id_recomendacion_personal = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    nombre_contacto = db.Column(db.String(100), nullable=False)
    telefono_contacto = db.Column(db.String(20), nullable=False)
    telefono_contacto2 = db.Column(db.String(20))
    id_persona = db.Column(db.ForeignKey('bt_persona.id_persona', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona = db.relationship('BtPersona', primaryjoin='BtRecomendacionPersonal.id_persona == BtPersona.id_persona', backref='bt_recomendacion_personals')



class BtRedSocial(db.Model):
    __tablename__ = 'bt_red_social'

    id_red_social = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    red_social = db.Column(db.String(50), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtRespPa(db.Model):
    __tablename__ = 'bt_resp_pa'

    id_resp_pa = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    respuesta = db.Column(db.String(250), nullable=False)
    ponderacion = db.Column(db.BigInteger, nullable=False)
    id_preg_pa = db.Column(db.ForeignKey('bt_preg_pa.id_preg_pa', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False)

    bt_preg_pa = db.relationship('BtPregPa', primaryjoin='BtRespPa.id_preg_pa == BtPregPa.id_preg_pa', backref='bt_resp_pas')



class BtRespPcOm(db.Model):
    __tablename__ = 'bt_resp_pc_om'

    id_resp_pc_om = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    respuesta = db.Column(db.String(250), nullable=False)
    ponderacion = db.Column(db.BigInteger, nullable=False)
    id_preg_pc = db.Column(db.ForeignKey('bt_preg_pc.id_preg_pc', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False)

    bt_preg_pc = db.relationship('BtPregPc', primaryjoin='BtRespPcOm.id_preg_pc == BtPregPc.id_preg_pc', backref='bt_resp_pc_oms')



class BtRespP(db.Model):
    __tablename__ = 'bt_resp_ps'

    id_resp_ps = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    respuesta = db.Column(db.String(250), nullable=False)
    ponderacion = db.Column(db.BigInteger, nullable=False)
    id_preg_ps = db.Column(db.ForeignKey('bt_preg_ps.id_preg_ps', ondelete='RESTRICT', onupdate='RESTRICT'))
    fecha_creacion = db.Column(db.DateTime(True), nullable=False)

    bt_preg_p = db.relationship('BtPregP', primaryjoin='BtRespP.id_preg_ps == BtPregP.id_preg_ps', backref='bt_resp_ps')



class BtRol(db.Model):
    __tablename__ = 'bt_rol'

    id_rol = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    rol = db.Column(db.String(50), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())



class BtTipoLogro(db.Model):
    __tablename__ = 'bt_tipo_logro'

    id_tipo_logro = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    id_persona_x_logro = db.Column(db.ForeignKey('bt_persona_x_logro.id_persona_x_logro', ondelete='RESTRICT', onupdate='RESTRICT'))
    tipo_logro = db.Column(db.String(200), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_persona_x_logro = db.relationship('BtPersonaXLogro', primaryjoin='BtTipoLogro.id_persona_x_logro == BtPersonaXLogro.id_persona_x_logro', backref='bt_tipo_logroes')



class BtUsuario(db.Model):
    __tablename__ = 'bt_usuario'

    id_usuario = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    nombres = db.Column(db.String(200), nullable=False)
    apellidos = db.Column(db.String(200), nullable=False)
    id_rol = db.Column(db.ForeignKey('bt_rol.id_rol', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    password = db.Column(db.String(500), nullable=False)
    correo_electronico = db.Column(db.String(50), nullable=False)
    id_estado_usuario = db.Column(db.ForeignKey('bt_estado_usuario.id_estado_usuario', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    fecha_creacion = db.Column(db.DateTime(True), nullable=False, server_default=db.FetchedValue())

    bt_estado_usuario = db.relationship('BtEstadoUsuario', primaryjoin='BtUsuario.id_estado_usuario == BtEstadoUsuario.id_estado_usuario', backref='bt_usuarios')
    bt_rol = db.relationship('BtRol', primaryjoin='BtUsuario.id_rol == BtRol.id_rol', backref='bt_usuarios')
